
	$(function(){
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();
		$('.carousel').carousel({
			interval:2000
		});
	});

	$("#contacto").on('show.bs.modal',function(e){
		console.log('Comenzo a abrirse el modal');
		$('.btn-restaurante').prop('disabled', true );
		$('.btn-restaurante').removeClass('btn-info');
		$('.btn-restaurante').addClass('btn-default');
	});

	$("#contacto").on('shown.bs.modal',function(e){
		console.log('Se ha abierto el modal');
		
	});

	$("#contacto").on('hide.bs.modal',function(e){
		console.log('Comenzo a cerrarse el modal');
		$('.btn-restaurante').prop('disabled', false );
		$('.btn-restaurante').removeClass('btn-default');
		$('.btn-restaurante').addClass('btn-info');
	});

	$("#contacto").on('hidden.bs.modal',function(e){
		console.log('Se ha cerrado el modal');
		
	});
